﻿namespace API.Dtos.Registration
{
    public class DataForRemovingRegistrationByAdminDto
    {
        public int ExcelId { get; set; }
        public int EventId { get; set; }
    }
}